Name:           bees
Version:        0.10
Release:        1%{?dist}
Summary:        Best-Effort Extent-Same, a btrfs dedup agent

License:        GPLv3
URL:            https://github.com/Zygo/bees
Source0:        %{url}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:  gcc-c++
BuildRequires:  make
BuildRequires:  pkgconfig(libbtrfsutil)
BuildRequires:  pkgconfig(uuid)
BuildRequires:  systemd


%description
bees is a block-oriented userspace deduplication agent designed for large btrfs
filesystems. It is an offline dedupe combined with an incremental data scan
capability to minimize time data spends on disk from write to dedupe.


%prep
%autosetup -p1


%build
%make_build BEES_VERSION=%{version} LIBDIR=%{_lib}


%install
%make_install BEES_VERSION=%{version} LIBDIR=%{_lib}


%files
%license COPYING
%doc README.md
%{_sbindir}/beesd
%{_libdir}/%{name}/%{name}
%{_unitdir}/beesd@.service
%config %{_sysconfdir}/%{name}/beesd.conf.sample


%changelog
* Mon Dec 25 2023 Timothy Redaelli <tredaelli@redhat.com> - 0.10-1
- New package

